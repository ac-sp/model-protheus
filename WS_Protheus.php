<?php

namespace Model\Protheus;

trait WS_Protheus{
    use \doctrine\Dashes\Model;
    public $wsdl;
    public $wsdl_assoc;
    public $client;
    public $client_assoc;
    
    public function __construct() {        
        $this->wsdl = "http://172.42.1.74:8087/ws/CFGTABLE.apw?WSDL"; //ip de producao
        $this->wsdl_assoc = "http://172.42.1.74:8087/ws/WSADDASSOCIADOS.apw?WSDL"; //ip de producao
//        $this->wsdl = "http://172.42.1.64:8075/ws01/CFGTABLE.apw?WSDL"; //ip de homologacao
//        $this->wsdl_assoc = "http://172.42.1.64:8075/ws01/WSADDASSOCIADOS.apw?WSDL"; //ip de homologacao
        
        $this->client = new \SoapClient($this->wsdl,['trace' => 1,'exceptions' => 0, 'soap_version'=>SOAP_1_1]);     
        $this->client_assoc = new \SoapClient($this->wsdl_assoc,['trace' => 1,'exceptions' => 0, 'soap_version'=>SOAP_1_1]);      
        
    }
    public function _parameters($alias, $query, $listFields) {
        return $parameters = [
            'GETTABLE' => [
                    'USERCODE' => 'MSALPHA',
                    'ALIAS' => $alias,
                    'QUERYADDWHERE' => $query,
                   'LISTFIELDSVIEW' => implode(",",$listFields)
            ]
        ];
    }
    public function _formatResponse($object) {
        $array_fields = [];
        $array_data = [];
        foreach ($object->TABLESTRUCT->FIELDSTRUCT as $fields) {  
            $array_fields[] = trim($fields->FLDNAME);
        }
        if(is_array($object->TABLEDATA->FIELDVIEW)) {
            foreach($object->TABLEDATA->FIELDVIEW as $convert) {            
                $data[] = array_combine($array_fields, $convert->FLDTAG->STRING);
            }
        } else {
            foreach($object->TABLEDATA->FIELDVIEW->FLDTAG as $convert) { 
                $data = array_combine($array_fields, $convert);
            }
        }
        return $data;
    }
}