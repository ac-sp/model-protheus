<?php

namespace Model\Protheus;

class YB1{
    use \Model\Protheus\WS_Protheus;
    
    public function dadosProspect($cpfcnpj) {
        $alias = 'YB1';
        $query = "YB1_COS={$cpfcnpj}";
        $listFields = [
            'YB1_FILIAL',
            'YB1_CODIGO',
            'YB1_NRSOLI',
            'YB1_TPAPRV',
            'YB1_ORIGEM',
            'YB1_TPOP',
            'YB1_COS',
            'YB1_CODPRI',
            'YB1_CLASSI',
            'YB1_CGC',
            'YB1_PESSOA',
            'YB1_NOME',
            'YB1_NREDUZ',
            'YB1_CONTAT',
            'YB1_CARGO',
            'YB1_ECONTA',
            'YB1_ENTIDA',
            'YB1_ASSOCI',
            'YB1_PONTEI',
            'YB1_FEDERA',
            'YB1_CLACSP',
            'YB1_X_COBR',
            'YB1_TIPLOG',
            'YB1_END',
            'YB1_COMPL',
            'YB1_NUMERO',
            'YB1_BAIRRO',
            'YB1_MUN',
            'YB1_EST',
            'YB1_CEP',
            'YB1_DDD',
            'YB1_TEL',
            'YB1_CEL',
            'YB1_EMAIL',
            'YB1_DISTRI',
            'YB1_DATCAD',
            'YB1_DTUALT',
            'YB1_CNAE',
            'YB1_NASSOC',
            'YB1_QTDFUN',
            'YB1_TPFILI',
            'YB1_DFILIA',
            'YB1_STATUS',
            'YB1_OK',
            'YB1_FATURA',
            'YB1_CLASSO',
            'YB1_OBS',
            'YB1_PRICNP',
            'YB1_XREPRE',
            'YB1_CONDPG',
            'YB1_PRENOV',
            'YB1_INSCR',
            'YB1_ROTULO',
            'YB1_CODPA1',
            'D_E_L_E_T_',
            'R_E_C_N_O_',
            'YB1_PRDREF',
            'YB1_ENDIST'
        ];
        $parameters = $this->_parameters($alias,$query,$listFields);       
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'prospect nao encontrado'];
        }
        return ['status' => 1, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];
    }
    public function adicionaprospect($dados) {
        $parameters = ['ASSOCIADOPRIMARIO' => $dados];
        $response = $this->client_assoc->ADDPRIMARIO($parameters);       
        return $response;
    }
    /*
     * Verifica se existe no protheus alguma solicitacao pendente de alteracao de cadastro ou alteracao de 
     * endereco. se positivo, não deve ser enviada outra solicitacao
     * @param string $tipo ['cadastral, 'endereco']
     * @param string $codcos
     * return array status
     */
    public function verificasolicitacaopendente($tipo , $codcos) {
        $tpaprov = ['cadastral' => "2","endereco" => "3"];
        $alias = 'YB1';
        $query = "YB1_COS={$codcos}";
        $listFields = ['YB1_OK','YB1_TPAPRV'];
        $parameters = $this->_parameters($alias,$query,$listFields);       
        $result = $this->client_assoc->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'prospect nao encontrado'];
        }
        $response = $this->_formatResponse($result->GETTABLERESULT);
        if(empty($response['YB1_OK']) && $response['YB1_TPAPRV'] == $tpaprov[$tipo]) {
            return ['status' => 1, 'msg' => "solicitacao {$tipo} pendente"];
        }
        return ['status' => 0, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];           
    }    
    
    public function addSolicEndereco($dados)
    {        
        $parameters = ['SOLICITACAOENDERECO' => $dados];
        $response = $this->client_assoc->ADDENDERECO($parameters);       
        return $response;        
    }
    
}