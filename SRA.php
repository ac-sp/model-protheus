<?php

namespace Model\Protheus;

class SRA{
    use \Model\Protheus\WS_Protheus;
 
    public function dadosFuncionario($cpf) {
        $alias = 'SRA';
        $query = "RA_CIC={$cpf}";
        $listFields = ['RA_FILIAL','RA_MAT','RA_NOME','RA_SITFOLH','RA_ADMISSA','RA_SEXO','RA_CC','RA_CODFUNC','RA_CIC','RA_EMAIL','RA_NASC'];
        $parameters = $this->_parameters($alias,$query,$listFields);        
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'funcionario nao encontrado'];            
        }        
        return $this->_formatResponse($result->GETTABLERESULT);
    }   

}