<?php

namespace Model\Protheus;

class PA1{
    use \Model\Protheus\WS_Protheus;
    private $alias = 'PA1';
    public function enderecos($codcos) {
        
        $query = "PA1_CODCOS='{$codcos}'";
        $listFields = [''];
        $parameters = $this->_parameters($this->alias,$query,$listFields);       
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'enderecos nao encontrados'];
        }
        return ['status' => 1, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];        
    }
}