<?php

namespace Model\Protheus;

class SE1{
    use \Model\Protheus\WS_Protheus;
 
    public $alias = 'SE1';
    public function titulosabertos($codcos) {
        $query = "E1_CODCOS='{$codcos}' AND E1_BAIXA = '' AND E1_NATUREZ = '01102' AND E1_EMISSAO <= '".date('Ymd')."'";
        $listFields = ['E1_PREFIXO','E1_NUM','E1_CODCOS','E1_PARCELA','E1_EMISSAO','E1_BAIXA','E1_PORTADO'];
        $parameters = $this->_parameters($this->alias,$query,$listFields);       
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'titulos nao encontrados para esse associado'];
        }
        return ['status' => 1, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];
    }
    public function titulo($prefixo, $numero, $parcela) {
        $query = "E1_CODCOS='{$prefixo}' AND E1_NUM = '{$numero}' AND E1_PARCELA = '{$parcela}'";
        $listFields = ['E1_PREFIXO','E1_NUM','E1_PARCELA','E1_CODCOS','E1_EMISSAO','E1_BAIXA','E1_PORTADO'];
        $parameters = $this->_parameters($this->alias,$query,$listFields);       
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'titulo nao encontrado para esse associado'];
        }
        return ['status' => 1, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];
    }
}