<?php

namespace Model\Protheus;

class SA1{
    use \Model\Protheus\WS_Protheus;
 
    public function dadosAssociado($codcos) {
        $alias = 'SA1';
        $query = "A1_CODCOS={$codcos}";
        $listFields = [
            'A1_FILIAL',
            'A1_CODCOS',
            'A1_COD',
            'A1_LOJA',
            'A1_TIPO',
            'A1_PESSOA',
            'A1_CGC',
            'A1_ENTIDAD',
            'A1_ASSOCIA',
            'A1_PONTEIR',
            'A1_FEDERAC',
            'A1_CLIACSP',
            'A1_CONDCOS',
            'A1_NOME',
            'A1_NREDUZ', //Nome fantasia
            'A1_X_COBR', //Tipo de cobranca
            'A1_TIPLOG',
            'A1_END',
            'A1_NUMERO',
            'A1_PAIS',
            'A1_COMPL',
            'A1_BAIRRO',
            'A1_CEP',
            'A1_MUN',
            'A1_EST',
            'A1_DDD',
            'A1_TEL',
            'A1_ENDCOB',
            'A1_ENDENT',
            'A1_ENDREC',
            'A1_INSCR', //Inscricao estadual
            'A1_ATIVIDA',
            'A1_TIPCLI',
            'A1_CONTATO',
            'A1_CARGO',
            'A1_EMAIL',
            'A1_DATCAD',
            'A1_DTUALT',
            'A1_CNAE',
            'A1_XPROSPE',
            'A1_XCEL',
            'A1_XREPRE',
            'A1_XCLASSE',
            'A1_XSOLIC',
            'A1_XBQACCL',
            'A1_XPRDREF',
            'A1_HRCAD',
            'A1_DTCAD'];
        $parameters = $this->_parameters($alias,$query,$listFields);       
        $result = $this->client->__soapCall("GETTABLE",$parameters);
        if (is_soap_fault($result)) {
            return ['status' => 0, 'msg' => 'associado nao encontrado'];
        }
        return ['status' => 1, 'dados' => $this->_formatResponse($result->GETTABLERESULT)];
    }
   

}